<?php
/**
 * @file
 * campaign_webform.features.uuid_entities.inc
 */

/**
 * Implements hook_uuid_default_entities().
 */
function campaign_webform_uuid_default_entities() {
  $entities = array();

  $entities['campaign_webform'][] = (object) array(
    '__metadata' => array(
      'type' => 'node',
      'uri' => 'node/e9784a69-98a2-0e64-15d4-792db95a2bcc',
      'cause' => FALSE,
    ),
    'body' => array(
      'und' => array(
        0 => array(
          'format' => 'plain_text',
          'summary' => '',
          'value' => 'LOPA, the so-called "Loving Our Pets Act," would be the end of the Internet as we know it.

If this law passes, anyone caught publishing cute cat pictures online would be sent away to federal prison for 20 years to life!

Don\'t let Congress send us back to the Dark Ages of publishing cute cat pictures with copy machines in back alleys.

If LOPA passes, not only will cute cat pictures be illegal, but what will they ban next? Soon pictures of cute puppies, cute ducklings, and all other cute pets will be made illegal! And, why should the government get to decide which cats are cute anyways?

Add your name to our petition, and we\'ll hand-deliver it to Congress!',
        ),
      ),
    ),
    'comment' => '0',
    'language' => 'und',
    'log' => '',
    'path' => array(
      'alias' => 'take-action',
      'pathauto' => FALSE,
    ),
    'promote' => '1',
    'status' => '1',
    'sticky' => '0',
    'title' => 'Take Action to Stop LOPA',
    'tnid' => '0',
    'translate' => '0',
    'type' => 'webform',
    'uid' => '1',
    'uuid' => 'e9784a69-98a2-0e64-15d4-792db95a2bcc',
    'webform' => array(
      'nid' => '1',
      'confirmation' => '',
      'confirmation_format' => 'plain_text',
      'redirect_url' => '<confirmation>',
      'status' => '1',
      'block' => '0',
      'teaser' => '0',
      'allow_draft' => '0',
      'auto_save' => '0',
      'submit_notice' => '0',
      'submit_text' => 'Sign and send to Congress!',
      'submit_limit' => '-1',
      'submit_interval' => '-1',
      'total_submit_limit' => '-1',
      'total_submit_interval' => '-1',
      'record_exists' => TRUE,
      'roles' => array(
        0 => '1',
        1 => '2',
      ),
      'emails' => array(),
      'components' => array(
        1 => array(
          'nid' => 1,
          'cid' => '1',
          'pid' => '0',
          'form_key' => 'name',
          'name' => 'Name',
          'type' => 'textfield',
          'value' => '',
          'extra' => array(
            'title_display' => 'before',
            'private' => 0,
            'disabled' => 0,
            'unique' => 0,
            'conditional_operator' => '=',
            'width' => '',
            'maxlength' => '',
            'field_prefix' => '',
            'field_suffix' => '',
            'description' => '',
            'attributes' => array(),
            'conditional_component' => '',
            'conditional_values' => '',
          ),
          'mandatory' => '1',
          'weight' => '0',
          'page_num' => 1,
        ),
        2 => array(
          'nid' => 1,
          'cid' => '2',
          'pid' => '0',
          'form_key' => 'e_mail',
          'name' => 'E-mail',
          'type' => 'email',
          'value' => '',
          'extra' => array(
            'title_display' => 'before',
            'private' => 0,
            'disabled' => 0,
            'unique' => 0,
            'conditional_operator' => '=',
            'width' => '',
            'description' => '',
            'attributes' => array(),
            'conditional_component' => '',
            'conditional_values' => '',
          ),
          'mandatory' => '1',
          'weight' => '1',
          'page_num' => 1,
        ),
        3 => array(
          'nid' => 1,
          'cid' => '3',
          'pid' => '0',
          'form_key' => 'subscribe_to_our_list',
          'name' => 'Subscribe to our list',
          'type' => 'select',
          'value' => '',
          'extra' => array(
            'items' => '1|Yes, please send me important updates about LOPA!',
            'multiple' => 1,
            'title_display' => 'before',
            'private' => 0,
            'aslist' => 0,
            'optrand' => 0,
            'conditional_operator' => '=',
            'other_option' => NULL,
            'other_text' => 'Other...',
            'description' => '',
            'custom_keys' => FALSE,
            'options_source' => '',
            'conditional_component' => '',
            'conditional_values' => '',
          ),
          'mandatory' => '0',
          'weight' => '2',
          'page_num' => 1,
        ),
        4 => array(
          'nid' => 1,
          'cid' => '4',
          'pid' => '0',
          'form_key' => 'postal_code',
          'name' => 'Zip code',
          'type' => 'textfield',
          'value' => '',
          'extra' => array(
            'title_display' => 'before',
            'private' => 0,
            'width' => '10',
            'disabled' => 0,
            'unique' => 0,
            'maxlength' => '5',
            'conditional_operator' => '=',
            'field_prefix' => '',
            'field_suffix' => '',
            'description' => '',
            'attributes' => array(),
            'conditional_component' => '',
            'conditional_values' => '',
          ),
          'mandatory' => '1',
          'weight' => '3',
          'page_num' => 1,
        ),
        5 => array(
          'nid' => 1,
          'cid' => '5',
          'pid' => '0',
          'form_key' => 'cute_cat_photo',
          'name' => 'Cute cat picture',
          'type' => 'file',
          'value' => '',
          'extra' => array(
            'scheme' => 'public',
            'title_display' => 'before',
            'private' => 0,
            'progress_indicator' => 'throbber',
            'filtering' => array(
              'size' => '2 MB',
              'types' => array(
                0 => 'jpg',
              ),
              'addextensions' => '',
            ),
            'conditional_operator' => '=',
            'directory' => '',
            'description' => 'Optionally upload your favorite cute cat picture, and we\'ll deliver it to Congress with the petition!',
            'attributes' => array(),
            'conditional_component' => '',
            'conditional_values' => '',
          ),
          'mandatory' => '0',
          'weight' => '4',
          'page_num' => 1,
        ),
        6 => array(
          'nid' => 1,
          'cid' => '6',
          'pid' => '0',
          'form_key' => 'state',
          'name' => 'State',
          'type' => 'textfield',
          'value' => '',
          'extra' => array(
            'title_display' => 'before',
            'private' => 1,
            'width' => '4',
            'disabled' => 0,
            'unique' => 0,
            'maxlength' => '2',
            'conditional_operator' => '=',
            'field_prefix' => '',
            'field_suffix' => '',
            'description' => '',
            'attributes' => array(),
            'conditional_component' => '',
            'conditional_values' => '',
          ),
          'mandatory' => '0',
          'weight' => '5',
          'page_num' => 1,
        ),
      ),
    ),
  );

  return $entities;
}
