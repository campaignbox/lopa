/**
 * @file
 * Javascript related to the main view list.
 */
(function ($) {

Drupal.behaviors.campaignStateTopThree = {
  attach: function (context) {
    $('#campaign-state').once('campaign-state-processed').each(function() {
      var refreshId = setInterval(function() {
        // Post some data to avoid caching.
        $('#campaign-state').load(Drupal.settings.basePath + 'sites/default/files/campaign_state.html', { refresh: 1 });
      }, 2000);
    });
  }
};

})(jQuery);
