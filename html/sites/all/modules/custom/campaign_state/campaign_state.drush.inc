<?php

/**
 * @file
 * Provides Drush integration for campaign_state.
 */

/**
 * Implements hook_drush_command().
 */
function campaign_state_drush_command() {
  $commands = array();

  $commands['campaign-state-regenerate'] = array(
    'description' => 'Regenerate view of top states every two seconds.',
    'drupal dependencies' => array('campaign_state'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'examples' => array(
      'drush campaign-state-regenerate' => 'Regenerate view of top states every two seconds.',
    ),
    'aliases' => array('campaign-state'),
  );

  return $commands;
}

/**
 * Implements hook_drush_help().
 */
function campaign_state_drush_help($section) {
  switch ($section) {
    case 'drush:campaign-state-regenerate':
      return dt('Regenerate view of top states every two seconds.');
  }
}

/**
 * Command callback.
 */
function drush_campaign_state_regenerate() {
  // Initialize variables.
  $success = TRUE;
  $old_output = '';

  while ($success) {
    $output = campaign_state_render_top_three();  
    // Only write if output has changed.
    if ($output !== $old_output) {
      $success = file_unmanaged_save_data($output, 'public://campaign_state.html', FILE_EXISTS_REPLACE);
    }
    $old_output = $output;
    sleep(2);
  }

  drush_set_error('Error encountered while saving cache file. Mostly likely this user does not have permission to write to the files directory.');
}
