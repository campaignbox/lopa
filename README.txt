DEPENDENCIES
------------

Typical Drupal 7 dependencies.
Drush is also required.

INSTALLATION
------------

Install as you normally would install Drupal, and make sure to choose 
the "Campaign" install profile.

SERVER-SIDE CACHE REGENERATION SCRIPT
-------------------------------------

To continually regenerate the static cache of the state signature 
count, run this Drush command on your server:
$ drush campaign-state regenerate
This command will run forever, checking for new data via an optimized 
database query, and if necessary, regenerating the cached HTML file, 
every two seconds. The user running this command will need write access 
to sites/default/files.

STATE FIELD ONLY VISIBLE TO ADMINISTRATORS
------------------------------------------

To view this app as intended you need to be logged out, not logged in 
as an administrator. For example, you could open up a different browser 
which is not logged in. State is a hidden field visible only to 
administrators.

PRIVACY
-------

Webform module normally logs IP addresses, but the custom module 
anonymizes the client IP address. In addition, the form is tagged as 
requiring HTTPS. If an HTTPS site is setup on the server, and a module 
from drupal.org which I maintain called Secure Login module is 
installed, the form will automatically redirect to HTTPS.
